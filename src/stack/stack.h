//Simple linked list nodes for Stack implementation

typedef struct Node Node;

struct Node
{
	void* data;
	Node* next;
};

Node* createNode(void* data, Node* next);
void deleteNode(Node* node);

//Implementing a stack in C using good practice™

typedef struct Stack Stack;

struct Stack
{
	unsigned int capacity;
	unsigned int size;
	Node* top;
};

Stack* createStack(int capacity);
void deleteStack(Stack* stack);
void push(Stack* stack, void* data);
void* pop(Stack* stack);
void* peek(Stack* stack);
int empty(Stack* stack);
int full(Stack* stack);