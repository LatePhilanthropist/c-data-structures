#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

Node *createNode(void* data, Node* next) {
	Node* node = malloc(sizeof(Node));
	assert(node != NULL);

	node->data = data;
	node->next = next;
	return node;
}

void deleteNode(Node* node) {
	free(node);
}

Stack* createStack(int capacity) {
	Stack* stack = malloc(sizeof(Stack));

	stack->capacity = capacity;
	stack->size = 0;
	stack->top = NULL;

	return stack;
}

void deleteStack(Stack* stack) {
	while(stack->top != NULL) {
		Node* toDelete = stack->top;
		stack->top = toDelete->next;
		deleteNode(toDelete);
	}
	free(stack);
}

void push(Stack* stack, void* data) {
	if(stack->size + 1 > stack->capacity) {
		printf("Reached max capacity. Cannot add more data.");
		return;
	}

	Node* toAdd = createNode(data, stack->top);
	stack->top = toAdd;
	stack->size++;
}

void* pop(Stack* stack) {
	void* data = stack->top->data;

	Node *toDelete = stack->top;	
	stack->top = toDelete->next;
	deleteNode(toDelete);
	stack->size--;

	return data;
}

void* peek(Stack* stack) {
	return stack->top->data;
}

int main(int argc, char *argv) {
	Stack* stack = createStack(100);

	int one = 1;
	push(stack, &one);

	int two = 2;
	push(stack, &two);

	char* hello = "hello";
	push(stack, hello);

	char* peeked = (char*)peek(stack);
	char* popped = (char*)pop(stack);

	printf("%s", popped);
	printf("%d", *(int*)peek(stack));

	deleteStack(stack);
	return 0;
}